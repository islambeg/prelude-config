(prelude-require-packages '(clj-refactor flycheck-clj-kondo))

(defun my-clojure-mode-hook ()
  (clj-refactor-mode 1)
  (yas-minor-mode 1) ; for adding require/use/import statements
  ;; This choice of keybinding leaves cider-macroexpand-1 unbound
  (cljr-add-keybindings-with-prefix "C-c C-m")
  (rainbow-delimiters-mode 1)
  (hs-minor-mode 1)
  (local-set-key (kbd "C-u C-s") 'swiper-isearch-thing-at-point))

(add-hook 'clojure-mode-hook #'my-clojure-mode-hook)

(defun my-clojure-on-save-hook ()
  (when (derived-mode-p 'clojure-mode) (cider-format-buffer)))

;; (add-hook 'before-save-hook #'my-clojure-on-save-hook)

(setq projectile-create-missing-test-files 1)


;; Cool helpers by @KGOH
(defun clj-insert-persist-scope-macro ()
  (interactive)
  (insert
   "(defmacro persist-scope
              \"Takes local scope vars and defines them in the global scope. Useful for RDD\"
              []
              `(do ~@(map (fn [v] `(def ~v ~v))
                  (keys (cond-> &env (contains? &env :locals) :locals)))))"))


(defun persist-scope ()
  (interactive)
  (let ((beg (point)))
    (clj-insert-persist-scope-macro)
    (cider-eval-region beg (point))
    (delete-region beg (point))
    (insert "(persist-scope)")
    (cider-eval-defun-at-point)
    (delete-region beg (point))))
