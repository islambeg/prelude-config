(defun random-alnum ()
  (let* ((alnum "abcdefghijklmnopqrstuvwxyz0123456789")
         (i (% (abs (random)) (length alnum))))
    (substring alnum i (1+ i))))

(defun random-alnum-string (num)
  (interactive)
  (dotimes (_ num)
    (insert
     (random-alnum))))
