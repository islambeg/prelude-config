(add-to-list 'default-frame-alist '(fullscreen . maximized))

(prelude-require-packages '(restclient))

(prelude-require-package 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

(prelude-require-package 'neil)
(setq neil-prompt-for-version-p nil
      neil-inject-dep-to-project-p t)
