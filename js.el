(prelude-require-packages '(prettier add-node-modules-path))

(defun add-custom-js-keys ()
  (local-set-key (kbd "C-c C-p") prettier-prettify-region))

(add-hook 'js2-mode-hook 'prettier-mode)
(add-hook 'js2-mode-hook 'add-node-modules-path)
(add-hook 'js2-mode-hook #'add-custom-js-keys)

(setq js2-mode-show-parse-errors (not js2-mode-show-parse-errors)
      js2-mode-show-strict-warnings (not js2-mode-show-strict-warnings))

(defun node-repl () (interactive)
       (setenv "NODE_NO_READLINE" "1") ;avoid fancy terminal codes
       (pop-to-buffer (make-comint "node-repl" "node" nil "--interactive")))
